package com.browserstack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by abhishek.sing on 09/09/17.
 */
public class Mac extends Os {

    @Override
    public void printCPUName() throws IOException, InterruptedException {
        Runtime run = Runtime.getRuntime();
        String bashcommand = "sysctl -n machdep.cpu.brand_string";
        Process pr = run.exec(bashcommand);
        pr.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line=buf.readLine();
        while ( line!= null )
        {
            System.out.println(line);
            line=buf.readLine();
        }

    }

    @Override
    public void printMemoryOfSystem() throws IOException, InterruptedException {
        Runtime run = Runtime.getRuntime();
        String bashcommand = "sysctl hw.memsize";
        Process pr = run.exec(bashcommand);
        pr.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        StringBuffer sb = new StringBuffer();
        String line=buf.readLine();
        while ( line!= null )
        {
            sb.append(line);
            line=buf.readLine();
        }
        System.out.println((Long.parseLong(sb.toString().split(":")[1].trim())/(1024*1024*1024)) + " GB");
    }
}
