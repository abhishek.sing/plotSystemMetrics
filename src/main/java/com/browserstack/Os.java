package com.browserstack;

import com.sun.management.OperatingSystemMXBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;

/**
 * Created by abhishek.sing on 09/09/17.
 */
public abstract class Os {
    static OperatingSystemMXBean bean;
    static {
        bean = (OperatingSystemMXBean) java.lang.management.ManagementFactory.getOperatingSystemMXBean();
    }
    protected void runCommandAndPrintOutput(String[] bashcommand) throws IOException, InterruptedException {
        Runtime run = Runtime.getRuntime();
        Process pr = run.exec(bashcommand);
        pr.getErrorStream();
        pr.waitFor();
        StringBuffer sb=new StringBuffer();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line=buf.readLine();
        while ( line!= null )
        {
            sb.append(line);
            line=buf.readLine();
        }
        System.out.println(sb.toString().trim());
    }
    public abstract  void printCPUName() throws IOException, InterruptedException;

    public void  printOsName(){
        System.out.println(bean.getName());
    }

    public static String  getOsName(){
        return (bean.getName());
    }

    public void printOsVersion(){
        System.out.println(bean.getVersion());
    }

    public void  printCpuArchitecture() {
        System.out.println(bean.getArch());
    }

    public void printNumberOfCores() {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    public abstract void printMemoryOfSystem() throws IOException, InterruptedException;


    public void  printHardwareaAndIPAddress() throws SocketException, UnknownHostException {
        Enumeration<NetworkInterface> availableInterfaces = NetworkInterface.getNetworkInterfaces();
        String [] address = new String[3];
        while (availableInterfaces.hasMoreElements()) {
            NetworkInterface singleInterface = availableInterfaces.nextElement();
            //considering only running and not loopback interface
            if (singleInterface.isLoopback() || !singleInterface.isUp())
                continue;
            Enumeration<InetAddress> ipAddresses = singleInterface.getInetAddresses();
            while(ipAddresses.hasMoreElements()) {
                InetAddress singleIpAddr = ipAddresses.nextElement();
                if (singleIpAddr instanceof Inet4Address && singleIpAddr.getHostAddress().equals(InetAddress.getLocalHost().getHostAddress())) {
                    byte[] hardwareAddress = singleInterface.getHardwareAddress();
                    StringBuffer hardwareAddressInString = new StringBuffer();
                    for (int i = 0; i < hardwareAddress.length; i++) {
                        hardwareAddressInString.append(String.format("%02X%s", hardwareAddress[i], (i < hardwareAddress.length - 1) ? ":" : ""));
                    }
                    System.out.println("Primary Network Address : Interface Name: "+singleInterface.getDisplayName() + ",  Ip4 Address: " + singleIpAddr.getHostAddress()+ ", Physical Address: "+hardwareAddressInString);
                }
            }
        }
    }




}
