package com.browserstack;

import java.io.IOException;
/**
 * This code is tested on following Platform :
 *
 * 1) Linux : Debian, Centos, Ubuntu
 * 2) Mac : Mac Os Sierra
 * 3) Windows : Windows Vista
 *
 */

/**
 * Created by abhishek.sing on 09/09/17.
 */
public class systemMetrics {

    public static void main(String [] args) throws IOException, InterruptedException {
        Os osSystem = null;
        if(Os.getOsName().toLowerCase().contains("windows")){
            osSystem = new Windows();
        }else if(Os.getOsName().toLowerCase().contains("mac")){
            osSystem = new Mac();
        }else if(Os.getOsName().toLowerCase().contains("linux")){
            osSystem = new Linux();
        }
        osSystem.printOsName();
        osSystem.printOsVersion();
        osSystem.printCPUName();
        osSystem.printCpuArchitecture();
        osSystem.printNumberOfCores();
        osSystem.printMemoryOfSystem();
        osSystem.printHardwareaAndIPAddress();
    }
}
