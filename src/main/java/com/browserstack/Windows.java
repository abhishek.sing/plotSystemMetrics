package com.browserstack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by abhishek.sing on 09/09/17.
 */
public class Windows extends Os {
    @Override
    public void printCPUName() {
        System.out.println(System.getenv("PROCESSOR_IDENTIFIER"));
    }

    @Override
    public void printMemoryOfSystem() throws IOException, InterruptedException {
        Runtime run = Runtime.getRuntime();
        String bashcommand = "wmic computersystem get TotalPhysicalMemory";
        Process pr = run.exec(bashcommand);
        pr.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        StringBuffer sb = new StringBuffer();
        String line=buf.readLine();
        while ( line!= null )
        {
            sb.append(line);
            line=buf.readLine();
        }
        System.out.println(sb.toString());
    }

}
