package com.browserstack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by abhishek.sing on 09/09/17.
 */
public class Linux extends Os {


    @Override
    public void printCPUName() throws IOException, InterruptedException {
        String [] bashcommand = { "/bin/sh", "-c", "cat /proc/cpuinfo | grep 'model name' | uniq | awk -F ':' '{print $2;}'"} ;
        runCommandAndPrintOutput(bashcommand);
    }

    @Override
    public void printMemoryOfSystem() throws IOException, InterruptedException {
        String [] bashcommand = { "/bin/sh", "-c", "cat /proc/meminfo  |grep MemTotal | awk -F ':' '{ print $2; }'"} ;
        runCommandAndPrintOutput(bashcommand);
    }
}
